import random
import os
import warnings

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
warnings.filterwarnings("ignore")

from collections import defaultdict, deque
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.optimizers import Adam
import utils
from feature_extractors import SimpleExtractor, OnlyStateExtractor
import numpy as np


class QLearning:

    def __init__(self, environment, epsilon=1, epsilon_decay=0.998, epsilon_threshold=0.005, alpha=0.001, discount=0.991):
        self.q_values = {}
        self.environment = environment
        self.epsilon = epsilon
        self.alpha = alpha
        self.discount = discount
        self.epsilon_decay = epsilon_decay
        self.epsilon_threshold = epsilon_threshold

    def get_q_value(self, state, action):
        return self.q_values[(state, action)] if (state, action) in self.q_values else 0.

    def compute_value_from_q_values(self, state):
        actions = self.environment.get_legal_actions()
        scores = [self.get_q_value(state, action) for action in actions]
        return max(scores) if len(actions) else 0.

    def compute_action_from_q_values(self, state):
        max_value = self.compute_value_from_q_values(state)
        actions = self.environment.get_legal_actions()
        actions_score_pairs = [[action, self.get_q_value(state, action)] for action in actions]
        max_score_actions = [action for action, score in actions_score_pairs if score == max_value]
        return random.choice(max_score_actions) if len(max_score_actions) else None

    def get_action(self):
        state = self.environment.get_current_state()
        legal_actions = self.environment.get_legal_actions()
        return random.choice(legal_actions)\
            if utils.flip_coin(self.epsilon)\
            else self.compute_action_from_q_values(state)

    def game_done(self):
        pass

    def update(self, state, action, next_state, reward):
        if (state, action) not in self.q_values:
            self.q_values[(state, action)] = 0.
        self.q_values[(state, action)] = self.q_values[(state, action)] + self.alpha * (
                reward + self.discount * self.compute_value_from_q_values(next_state) - self.q_values[(state, action)])
        self.epsilon = max(self.epsilon * self.epsilon_decay, self.epsilon_threshold)


class ApproximateQLearning(QLearning):

    def __init__(self, extractor=SimpleExtractor(), **args):
        self.featExtractor = extractor
        QLearning.__init__(self, **args)
        self.weights = defaultdict(lambda: 1.)

    def get_weights(self):
        return self.weights

    def get_q_value(self, state, action):
        feature_vector = self.featExtractor.get_features(state, action)
        return sum([feature_vector[f] * self.weights[f] for f in feature_vector])

    def update(self, state, action, next_state, reward):
        difference = reward + self.discount * self.compute_value_from_q_values(next_state) - self.get_q_value(state, action)
        feature_vector = self.featExtractor.get_features(state, action)
        for feature in feature_vector:
            self.weights[feature] += self.alpha * difference * feature_vector[feature]
        self.epsilon = max(self.epsilon * self.epsilon_decay, self.epsilon_threshold)


class DeepQLearningWithMemory(QLearning):
    def __init__(self, extractor=OnlyStateExtractor(), **args):
        self.memory = deque(maxlen=5000)
        self.featExtractor = extractor
        self.time = 0
        self.max_last_hits = 0
        QLearning.__init__(self, **args)

        self.neural_network = Sequential()
        self.neural_network.add(Dense(32, input_dim=self.featExtractor.get_size(), activation="sigmoid"))
        self.neural_network.add(Dense(26, "linear"))
        self.neural_network.compile(loss="mse", optimizer=Adam(lr=self.alpha))

        if os.path.isfile("weights_deep.h5"):
            self.neural_network.load_weights("weights_deep.h5")

    def get_action(self):
        state = self.environment.get_current_state()
        legal_actions = self.environment.get_legal_actions()
        features = self.featExtractor.get_features(state, -1)

        action = -1

        if utils.flip_coin(self.epsilon):
            action = random.choice(legal_actions)
        else:
            predictions = utils.sorted_argmax(self.predict(features))
            for prediction in predictions:
                if prediction in legal_actions:
                    action = prediction
                    break
            if action >= 9:
                action += 4

        return action

    def predict(self, state):
        return self.neural_network.predict(np.reshape(state, (1, len(state))))[0]

    def game_done(self):
        self.time += 1
        self.max_last_hits = max(self.max_last_hits, self.environment.current_state["me"].last_hits)
        print(f'Iteration num: {self.time}\nMax last hits so far: {self.max_last_hits}')
        if len(self.memory) >= 50:
            batch = random.sample(self.memory, 50)
            states, targets = [], []
            for state, action, reward, next_state in batch:
                y1 = self.predict(state)
                y2 = self.predict(next_state)
                target = y1.tolist()
                target[action] = reward + (self.discount * max(y2))

                states.append(state)
                targets.append(target)

            self.neural_network.fit(states, targets, epochs=len(states), verbose=0)

            self.neural_network.save_weights("weights_deep.h5")

        self.epsilon = max(self.epsilon * self.epsilon_decay, self.epsilon_threshold)
        print(f'Epsilon: {self.epsilon}')

    def update(self, state, action, next_state, reward):
        if action != -1:
            self.memory.append((self.featExtractor.get_features(state, -1),
                                action if action < 9 else action - 4,
                                reward,
                                self.featExtractor.get_features(next_state, -1)))


class HierarchicalDeepQLearningWithMemory(QLearning):

    def __init__(self, extractor=OnlyStateExtractor(), **args):
        self.memory = deque(maxlen=5000)
        self.featExtractor = extractor
        self.time = 0
        self.max_last_hits = 0
        QLearning.__init__(self, **args)

        walking = Sequential()
        walking.add(Dense(16, input_dim=self.featExtractor.get_size(), activation="sigmoid"))
        walking.add(Dense(8, "linear"))
        walking.compile(loss="mse", optimizer=Adam(learning_rate=self.alpha))

        laning = Sequential()
        laning.add(Dense(16, input_dim=self.featExtractor.get_size(), activation="sigmoid"))
        laning.add(Dense(16, "linear"))
        laning.compile(loss="mse", optimizer=Adam(learning_rate=self.alpha))

        fighting = Sequential()
        fighting.add(Dense(16, input_dim=self.featExtractor.get_size(), activation="sigmoid"))
        fighting.add(Dense(5, "linear"))
        fighting.compile(loss="mse", optimizer=Adam(learning_rate=self.alpha))

        retreating = Sequential()
        retreating.add(Dense(16, input_dim=self.featExtractor.get_size(), activation="sigmoid"))
        retreating.add(Dense(9, "linear"))
        retreating.compile(loss="mse", optimizer=Adam(learning_rate=self.alpha))

        top = Sequential()
        top.add(Dense(32, input_dim=self.featExtractor.get_size(), activation="sigmoid"))
        top.add(Dense(4, "softmax"))
        top.compile(loss="mse", optimizer=Adam(learning_rate=self.alpha))

        self.neural_networks = [top, walking, laning, fighting, retreating]
        for i in range(len(self.neural_networks)):
            if os.path.isfile(f'weights_{i}.h5'):
                self.neural_networks[i].load_weights(f'weights_{i}.h5')

        self.last_strategy, self.last_prediction = -1, -1
        self.interpret_prediction = {1: lambda x: x + 1,
                                     2: lambda x: 0 if not x else x + 13,
                                     3: lambda x: 13 if not x else x + 24,
                                     4: lambda x: 29 if x == 8 else x + 1}
        self.find_strategy_and_prediction = {0: (2, 0), 1: (1, 0), 2: (1, 1), 3: (1, 2), 4: (1, 3), 5: (1, 4), 6: (1, 5), 7: (1, 6), 8: (1, 7),
                                             13: (3, 0), 14: (2, 1), 15: (2, 2), 16: (2, 3), 17: (2, 4), 18: (2, 5), 19: (2, 6), 20: (2, 7),
                                             21: (2, 8), 22: (2, 9), 23: (2, 10), 24: (2, 11), 25: (3, 1), 26: (3, 2), 27: (3, 3), 28: (3, 4),
                                             29: (4, 8)}

    def get_action(self):
        state = self.environment.get_current_state()
        legal_actions = self.environment.get_legal_actions()
        features = self.featExtractor.get_features(state, -1)

        if utils.flip_coin(self.epsilon):
            action = random.choice(legal_actions)
            self.last_strategy, self.last_prediction = self.find_strategy_and_prediction[action]
        else:
            strategies = utils.sorted_argmax(self.predict(0, features))
            strategy = -1
            prediction = -1
            for i in range(len(strategies)):
                strategy = strategies[i] + 1
                predictions = utils.sorted_argmax(self.predict(strategy, features))
                for j in range(len(predictions)):
                    if predictions[i] in legal_actions:
                        prediction = predictions[i]
                        break
                else:
                    continue
                break

            action = self.interpret_prediction[strategy](prediction) if prediction != -1 else legal_actions[0]
            self.last_strategy, self.last_prediction = strategy, prediction
        return action

    def predict(self, index, state):
        return self.neural_networks[index].predict(np.reshape(state, (1, len(state))))[0]

    def game_done(self):
        self.time += 1
        self.max_last_hits = max(self.max_last_hits, self.environment.current_state["me"].last_hits)
        print(f'Iteration num: {self.time}\nMax last hits so far: {self.max_last_hits}')
        if len(self.memory) >= 50:
            batch = random.sample(self.memory, 50)
            states, targets = [[] for _ in range(len(self.neural_networks))], [[] for _ in range(len(self.neural_networks))]
            for state, strategy, prediction, reward, next_state in batch:
                y1 = self.predict(0, state)
                y2 = self.predict(0, next_state)
                target0 = y1.tolist()
                target0[int(strategy)-1] = reward + (self.discount * max(y2))

                y3 = self.predict(strategy, state)
                y4 = self.predict(strategy, next_state)
                target = y3.tolist()
                target[int(prediction)] = reward + (self.discount * max(y4))

                states[0].append(state)
                states[strategy].append(state)
                targets[0].append(target0)
                targets[strategy].append(target)

            for i in range(len(states)):
                if len(states[i]):
                    self.neural_networks[i].fit(states[i], targets[i], epochs=len(states[i]), verbose=0)

            for i in range(len(self.neural_networks)):
                self.neural_networks[i].save_weights(f'weights_{i}.h5')

        self.epsilon = max(self.epsilon * self.epsilon_decay, self.epsilon_threshold)
        print(f'Epsilon: {self.epsilon}')

    def update(self, state, action, next_state, reward):
        if self.last_prediction != -1:
            self.memory.append((self.featExtractor.get_features(state, -1),
                                self.last_strategy,
                                self.last_prediction,
                                reward,
                                self.featExtractor.get_features(next_state, -1)))
