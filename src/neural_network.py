import math
import random


def sigmoid(x):
    return 1. / (1. + math.e ** -x)


''' 
use example:
NeuralNetwork(
        [2, 2, 1],
        [sigmoid, lambda x: x])
predict([1, 2])
'''
class NeuralNetwork:
    def __init__(self, layout, activation_func):
        self.layout = layout
        self.activation_func = activation_func
        self.weights = [random.random() for _ in range(self.weight_num())]

    def weight_num(self):
        return sum(self.layout[i] * (self.layout[i-1] + 1) for i in range(1, len(self.layout)))

    def predict(self, inputs):
        return []


class FeedForwardNeuralNetwork(NeuralNetwork):

    def __init__(self, layout, activation_func):
        super().__init__(layout, activation_func)

    def predict(self, inputs):
        w = []
        weights = [*self.weights]
        for i in range(1, len(self.layout)):
            node_weights = []
            for j in range(self.layout[i]):
                node_weights.append(weights[:self.layout[i-1]+1])
                weights = weights[self.layout[i-1]+1:]
            w.append(node_weights)

        output = []
        for layer in range(len(w)):
            output = []
            for node_weights in w[layer]:
                output.append(node_weights[0] +
                              sum(node_weights[i+1] * inputs[i]
                                  for i in range(len(inputs))))
                output[-1] = self.activation_func[layer](output[-1])
            inputs = output

        return output
