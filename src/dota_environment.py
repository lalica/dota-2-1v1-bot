from collections import namedtuple
from utils import distance, can_cast

Hero = namedtuple("Hero", ["level",
                           "health", "max_health", "health_regen",
                           "mana", "max_mana", "mana_regen",
                           "base_move_speed", "current_move_speed",
                           "attack_damage", "attack_range",
                           "attack_speed",
                           "last_attack_time", "attack_target",
                           "gold", "net_worth",
                           "last_hits", "denies",
                           "x", "y",
                           "ability_level",
                           "ability_mana_cost",
                           "ability_damage",
                           "ability_1_cast_range",
                           "ability_1_cooldown",
                           "ability_2_cast_range",
                           "ability_2_cooldown",
                           "ability_3_cast_range",
                           "ability_3_cooldown",
                           "ult_level",
                           "ult_mana_cost",
                           "ult_damage",
                           "ult_cast_range",
                           "ult_cooldown"])

Tower = namedtuple("Tower", ["health", "max_health",
                             "attack_damage", "attack_range",
                             "attack_speed",
                             "x", "y"])

Creep = namedtuple("Creep", ["health", "max_health",
                             "current_move_speed",
                             "attack_damage", "attack_range",
                             "attack_speed",
                             "x", "y"])

GameData = namedtuple("GameData", ["healing_salve_available",
                                   "fountain_x", "fountain_y",
                                   "middle_x", "middle_y"])

shadowraze_damage = {0: 0, 1: 90, 2: 160, 3: 230, 4: 300, 5: 300}
requiem_damage = {0: 0, 1: 80, 2: 120, 3: 160}

# not useful
bounty_rune_1_x, bounty_rune_1_y = -4122, -65
bounty_rune_2_x, bounty_rune_2_y = -4046, -2531
power_rune_1_x, power_rune_1_y = -1622, 968
power_rune_2_x, power_rune_2_y = 1196, -1276
day_vision_range = 1800
night_vision_range = 800


class SimpleDotaEnvironment:

    def __init__(self):
        self.actions = [*range(30)]
        self.current_state = {}
        self.previous_state = {}
        self.previous_action = -1

    def reset(self):
        self.current_state = {}
        self.previous_state = {}
        self.previous_action = -1

    def get_legal_actions(self):
        to_remove = [9, 10, 11, 12]
        if self.current_state["enemy_creeps"][0].health <= 0:
            to_remove.append(15)
        if self.current_state["enemy_creeps"][1].health <= 0:
            to_remove.append(16)
        if self.current_state["enemy_creeps"][2].health <= 0:
            to_remove.append(17)
        if self.current_state["enemy_creeps"][3].health <= 0:
            to_remove.append(18)
        if self.current_state["enemy_creeps"][4].health <= 0:
            to_remove.append(19)
        if self.current_state["ally_creeps"][0].health <= 0 or\
                self.current_state["ally_creeps"][0].health > self.current_state["ally_creeps"][0].max_health / 2:
            to_remove.append(20)
        if self.current_state["ally_creeps"][1].health <= 0 or\
                self.current_state["ally_creeps"][1].health > self.current_state["ally_creeps"][1].max_health / 2:
            to_remove.append(21)
        if self.current_state["ally_creeps"][2].health <= 0 or\
                self.current_state["ally_creeps"][2].health> self.current_state["ally_creeps"][2].max_health / 2:
            to_remove.append(22)
        if self.current_state["ally_creeps"][3].health <= 0 or\
                self.current_state["ally_creeps"][3].health > self.current_state["ally_creeps"][3].max_health / 2:
            to_remove.append(23)
        if self.current_state["ally_creeps"][4].health <= 0 or\
                self.current_state["ally_creeps"][4].health > self.current_state["ally_creeps"][4].max_health / 2:
            to_remove.append(24)
        if not can_cast(self.current_state["me"].ability_level,
                        self.current_state["me"].ability_mana_cost,
                        self.current_state["me"].mana,
                        self.current_state["me"].ability_1_cooldown):
            to_remove.append(25)
        if not can_cast(self.current_state["me"].ability_level,
                        self.current_state["me"].ability_mana_cost,
                        self.current_state["me"].mana,
                        self.current_state["me"].ability_2_cooldown):
            to_remove.append(26)
        if not can_cast(self.current_state["me"].ability_level,
                        self.current_state["me"].ability_mana_cost,
                        self.current_state["me"].mana,
                        self.current_state["me"].ability_3_cooldown):
            to_remove.append(27)
        if not can_cast(self.current_state["me"].ult_level,
                        self.current_state["me"].ult_mana_cost,
                        self.current_state["me"].mana,
                        self.current_state["me"].ult_cooldown):
            to_remove.append(28)
        if self.current_state["game_data"].healing_salve_available == -1:
            to_remove.append(29)
        return [a for a in self.actions if a not in to_remove]

    def get_all_actions(self):
        return self.actions

    def get_current_state(self):
        return self.current_state

    def extract_state_from_features(self, features):
        me = Hero(level=features[1],
                  health=features[2],
                  max_health=features[3],
                  health_regen=features[4],
                  mana=features[5],
                  max_mana=features[6],
                  mana_regen=features[7],
                  base_move_speed=features[8],
                  current_move_speed=features[9],
                  attack_damage=features[12],
                  attack_range=500,
                  attack_speed=features[14],
                  last_attack_time=features[17],
                  attack_target=features[18],
                  gold=features[22],
                  net_worth=features[23],
                  last_hits=features[24],
                  denies=features[25],
                  x=features[26],
                  y=features[27],
                  ability_level=features[225],
                  ability_mana_cost=features[226],
                  ability_damage=shadowraze_damage[features[225]],
                  ability_1_cast_range=features[228],
                  ability_1_cooldown=features[229],
                  ability_2_cast_range=features[235],
                  ability_2_cooldown=features[236],
                  ability_3_cast_range=features[242],
                  ability_3_cooldown=features[236],
                  ult_level=features[260],
                  ult_mana_cost=features[261],
                  ult_damage=requiem_damage[features[260]],
                  ult_cast_range=features[263],
                  ult_cooldown=features[264])
        enemy = Hero(level=features[31],
                     health=features[32],
                     max_health=features[33],
                     health_regen=features[34],
                     mana=features[35],
                     max_mana=features[36],
                     mana_regen=features[37],
                     base_move_speed=features[38],
                     current_move_speed=features[39],
                     attack_damage=features[42],
                     attack_range=500,
                     attack_speed=features[44],
                     last_attack_time=features[47],
                     attack_target=features[48],
                     gold=-1,
                     net_worth=-1,
                     last_hits=-1,
                     denies=-1,
                     x=features[52],
                     y=features[53],
                     ability_level=features[267],
                     ability_mana_cost=features[268],
                     ability_damage=shadowraze_damage[features[267]],
                     ability_1_cast_range=features[270],
                     ability_1_cooldown=features[271],
                     ability_2_cast_range=features[277],
                     ability_2_cooldown=features[278],
                     ability_3_cast_range=features[284],
                     ability_3_cooldown=features[285],
                     ult_level=features[302],
                     ult_mana_cost=features[303],
                     ult_damage=requiem_damage[features[302]],
                     ult_cast_range=features[305],
                     ult_cooldown=features[306])
        ally_tower = Tower(health=features[58],
                           max_health=features[59],
                           attack_damage=features[60],
                           attack_range=700,
                           attack_speed=features[62],
                           x=-1532,
                           y=-1410)
        enemy_tower = Tower(health=features[64],
                            max_health=features[65],
                            attack_damage=features[66],
                            attack_range=700,
                            attack_speed=features[68],
                            x=-537,
                            y=672)
        ally_creeps = []
        enemy_creeps = []
        for i in range(5):
            ac = Creep(health=features[86 + i * 14],
                       max_health=features[87 + i * 14],
                       current_move_speed=features[90 + i * 14],
                       attack_damage=features[93 + i * 14],
                       attack_range=500,
                       attack_speed=features[95 + i * 14],
                       x=features[97 + i * 14],
                       y=features[98 + i * 14])
            ec = Creep(health=features[156 + i * 14],
                       max_health=features[157 + i * 14],
                       current_move_speed=features[160 + i * 14],
                       attack_damage=features[163 + i * 14],
                       attack_range=500,
                       attack_speed=features[165 + i * 14],
                       x=features[167 + i * 14],
                       y=features[168 + i * 14])
            ally_creeps.append(ac)
            enemy_creeps.append(ec)
        game_data = GameData(healing_salve_available=features[309],
                             fountain_x=-6984,
                             fountain_y=-6455,
                             middle_x=-500,
                             middle_y=420)

        self.current_state = {"me": me,
                              "enemy": enemy,
                              "ally_tower": ally_tower,
                              "enemy_tower": enemy_tower,
                              "ally_creeps": ally_creeps,
                              "enemy_creeps": enemy_creeps,
                              "game_data": game_data}

    def get_reward(self, time):
        if not self.previous_state or self.previous_action == -1:
            return [], -1, [], 0
        reward = 0
        reward += self.current_state["me"].last_hits - self.previous_state["me"].last_hits
        reward += self.current_state["me"].denies - self.previous_state["me"].denies
        if self.current_state["enemy_tower"].health == 0 and \
                self.previous_state["enemy_tower"].health > 0:
            reward += 10
        if self.current_state["ally_tower"].health == 0 and \
                self.previous_state["ally_tower"].health > 0:
            reward += -10
        if self.current_state["enemy"].health == 0 and \
                self.previous_state["enemy"].health > 0:
            reward += 10
        if self.current_state["me"].health == 0 and \
                self.previous_state["me"].health > 0:
            reward += -5 if time < 500 else -10
        if self.current_state["me"].x == self.previous_state["me"].x and \
                self.current_state["me"].y == self.previous_state["me"].y and \
                self.previous_action in range(1, 9):
            reward += -0.1
        if distance(self.current_state["me"].x,
                    self.current_state["me"].y,
                    self.current_state["game_data"].middle_x,
                    self.current_state["game_data"].middle_y) > 1000:
            reward += -0.1 if time < 100 else -0.5
        return self.previous_state, self.previous_action, self.current_state, reward

# FEATURES:
#
# 0: team
# 1: level                                                                     *
# 2: health                                                                    *
# 3: max_health                                                                .
# 4: health_regen                                                              .
# 5: mana                                                                      *
# 6: max_mana                                                                  .
# 7: mana_regen                                                                .
# 8: base_move_speed
# 9: current_move_speed                                                        *
# 10: base_damage
# 11: damage_variance
# 12: attack_damage                                                            *
# 13: attack_range_buffer                                                      .
# 14: attack_speed                                                             *
# 15: seconds_per_attack
# 16: attack_animation_point
# 17: last_attack_time
# 18: attack_target                                                            *
# 19: strength
# 20: agility
# 21: intellect
# 22: gold
# 23: net_worth                                                                *
# 24: last_hits                                                                *
# 25: denies                                                                   *
# 26: location_1                                                               *
# 27: location_2                                                               *
# 28: facing
# 29: vision_range
#
# 30: opp team
# 31: opp_level                                                                *
# 32: opp_health                                                               *
# 33: opp_max_health                                                           .
# 34: opp_health_regen                                                         .
# 35: opp_mana                                                                 *
# 36: opp_max_mana                                                             .
# 37: opp_mana_regen                                                           .
# 38: opp_base_move_speed
# 39: opp_current_move_speed                                                   *
# 40: opp_base_damage
# 41: opp damage_variance
# 42: opp_attack_damage                                                        *
# 43: opp_attack_range_buffer
# 44: opp_attack_speed                                                         *
# 45: opp_seconds_per_attack
# 46: opp_attack_animation_point
# 47: opp_last_attack_time
# 48: opp_attack_target                                                        *
# 49: opp_strength
# 50: opp_agility
# 51: opp_intellect
# 52: opp_location_1                                                           *
# 53: opp_location_2                                                           *
# 54: opp_facing
# 55: opp_vision_range
#
#
# 56: dota_time                                                                .
#
#
# 57: good_tower_team
# 58: good_tower_health                                                        *
# 59: good tower_max_health
# 60: good_tower_attack_damage                                                 .
# 61: good_tower_attack_range_buffer
# 62: good_tower_attack_speed                                                  .
#
# 63: bad_tower_team
# 64: bad_tower_health                                                         *
# 65: bad tower_max_health
# 66: bad_tower_attack_damage                                                  .
# 67: bad_tower_attack_range_buffer
# 68: bad_tower_attack_speed                                                   .
#
#
# USELESS (fake data):
# 69: bounty_1_location_1
# 70: bounty_1_location_2
# 71: bounty_2_location_1
# 72: bounty_2_location_2
# 73: bounty_3_location_1
# 74: bounty_3_location_2
# 75: bounty_4_location_1
# 76: bounty_4_location_2
# 77: power_1_location_1
# 78: power_1_location_2
# 79: power_2_location_1
# 80: power_2_location_2
# 81: power_1_type
# 82: power_1_status
# 83: power_2_type
# 84: power_2_status
#
#
# 85: good_creep_1_team
# 86: good_creep_1_health                                                      *
# 87: good_creep_1_max_health
# 88: good_creep_1_health_regen
# 89: good_creep_1_base_move_speed
# 90: good_creep_1_current_move_speed
# 91: good_creep_1_base_damage
# 92: good_creep_1_damage_variance
# 93: good_creep_1_attack_damage
# 94: good_creep_1_attack_range_buffer
# 95: good_creep_1_attack_speed
# 96: good creep_1_seconds_per_attack
# 97: good_creep_1_location_1                                                  *
# 98: good_creep_1_location_2                                                  *
#
# 99: good_creep_2_team
# 100: good_creep_2_health                                                     *
# 101: good_creep_2_max_health
# 102: good_creep_2_health_regen
# 103: good_creep_2_base_move_speed
# 104: good_creep_2_current_move_speed
# 105: good_creep_2_base_damage
# 106: good_creep_2_damage_variance
# 107: good_creep_2_attack_damage
# 108: good_creep_2_attack_range_buffer
# 109: good_creep_2_attack_speed
# 110: good creep_2_seconds_per_attack
# 111: good_creep_2_location_1                                                 *
# 112: good_creep_2_location_2                                                 *
#
# 113: good_creep_3_team
# 114: good_creep_3_health                                                     *
# 115: good_creep_3_max_health
# 116: good_creep_3_health_regen
# 117: good_creep_3_base_move_speed
# 118: good_creep_3_current_move_speed
# 119: good_creep_3_base_damage
# 120: good_creep_3_damage_variance
# 121: good_creep_3_attack_damage
# 122: good_creep_3_attack_range_buffer
# 123: good_creep_3_attack_speed
# 124: good creep_3_seconds_per_attack
# 125: good_creep_3_location_1                                                 *
# 126: good_creep_3_location_2                                                 *
#
# 127: good_creep_4_team
# 128: good_creep_4_health                                                     *
# 129: good_creep_4_max_health
# 130: good_creep_4_health_regen
# 131: good_creep_4_base_move_speed
# 132: good_creep_4_current_move_speed
# 133: good_creep_4_base_damage
# 134: good_creep_4_damage_variance
# 135: good_creep_4_attack_damage
# 136: good_creep_4_attack_range_buffer
# 137: good_creep_4_attack_speed
# 138: good creep_4_seconds_per_attack
# 139: good_creep_4_location_1                                                 *
# 140: good_creep_4_location_2                                                 *
#
# 141: good_creep_5_team
# 142: good_creep_5_health                                                     *
# 143: good_creep_5_max_health
# 144: good_creep_5_health_regen
# 145: good_creep_5_base_move_speed
# 146: good_creep_5_current_move_speed
# 147: good_creep_5_base_damage
# 148: good_creep_5_damage_variance
# 149: good_creep_5_attack_damage
# 150: good_creep_5_attack_range_buffer
# 151: good_creep_5_attack_speed
# 152: good creep_5_seconds_per_attack
# 153: good_creep_5_location_1                                                 *
# 154: good_creep_5_location_2                                                 *
#
# 155: bad_creep_1_team
# 156: bad_creep_1_health                                                      *
# 157: bad_creep_1_max_health
# 158: bad_creep_1_health_regen
# 159: bad_creep_1_base_move_speed
# 160: bad_creep_1_current_move_speed
# 161: bad_creep_1_base_damage
# 162: bad_creep_1_damage_variance
# 163: bad_creep_1_attack_damage
# 164: bad_creep_1_attack_range_buffer
# 165: bad_creep_1_attack_speed
# 166: bad creep_1_seconds_per_attack
# 167: bad_creep_1_location_1                                                  *
# 168: bad_creep_1_location_2                                                  *
#
# 169: bad_creep_2_team
# 170: bad_creep_2_health                                                      *
# 171: bad_creep_2_max_health
# 172: bad_creep_2_health_regen
# 173: bad_creep_2_base_move_speed
# 174: bad_creep_2_current_move_speed
# 175: bad_creep_2_base_damage
# 176: bad_creep_2_damage_variance
# 177: bad_creep_2_attack_damage
# 178: bad_creep_2_attack_range_buffer
# 179: bad_creep_2_attack_speed
# 180: bad creep_2_seconds_per_attack
# 181: bad_creep_2_location_1                                                  *
# 182: bad_creep_2_location_2                                                  *
#
# 183: bad_creep_3_team
# 184: bad_creep_3_health                                                      *
# 185: bad_creep_3_max_health
# 186: bad_creep_3_health_regen
# 187: bad_creep_3_base_move_speed
# 188: bad_creep_3_current_move_speed
# 189: bad_creep_3_base_damage
# 190: bad_creep_3_damage_variance
# 191: bad_creep_3_attack_damage
# 192: bad_creep_3_attack_range_buffer
# 193: bad_creep_3_attack_speed
# 194: bad creep_3_seconds_per_attack
# 195: bad_creep_3_location_1                                                  *
# 196: bad_creep_3_location_2                                                  *
#
# 197: bad_creep_4_team
# 198: bad_creep_4_health                                                      *
# 199: bad_creep_4_max_health
# 200: bad_creep_4_health_regen
# 201: bad_creep_4_base_move_speed
# 202: bad_creep_4_current_move_speed
# 203: bad_creep_4_base_damage
# 204: bad_creep_4_damage_variance
# 205: bad_creep_4_attack_damage
# 206: bad_creep_4_attack_range_buffer
# 207: bad_creep_4_attack_speed
# 208: bad creep_4_seconds_per_attack
# 209: bad_creep_4_location_1                                                  *
# 210: bad_creep_4_location_2                                                  *
#
# 211: bad_creep_5_team
# 212: bad_creep_5_health                                                      *
# 213: bad_creep_5_max_health
# 214: bad_creep_5_health_regen
# 215: bad_creep_5_base_move_speed
# 216: bad_creep_5_current_move_speed
# 217: bad_creep_5_base_damage
# 218: bad_creep_5_damage_variance
# 219: bad_creep_5_attack_damage
# 220: bad_creep_5_attack_range_buffer
# 221: bad_creep_5_attack_speed
# 222: bad creep_5_seconds_per_attack
# 223: bad_creep_5_location_1                                                  *
# 224: bad_creep_5_location_2                                                  *
#
#
# 225: ability_1_level                                                         *
# 226: ability_1_mana_cost                                                     .
# 227: ability_1_ability_damage
# 228: ability_1_cast_range                                                    .
# 229: ability_1_cooldown_time_remaining                                       .
# 230: ability_1_target_type
# 231: ability_1_behavior
#
# 232: ability_2_level
# 233: ability_2_mana_cost
# 234: ability_2_ability_damage
# 235: ability_2_cast_range                                                    .
# 236: ability_2_cooldown_time_remaining                                       .
# 237: ability_2_target_type
# 238: ability_2_behavior
#
# 239: ability_3_level
# 240: ability_3_mana_cost
# 241: ability_3_ability_damage
# 242: ability_3_cast_range                                                    .
# 243: ability_3_cooldown_time_remaining                                       .
# 244: ability_3_target_type
# 245: ability_3_behavior
#
# 246: ability_4_level                                                         *
# 247: ability_4_mana_cost
# 248: ability_4_ability_damage
# 249: ability_4_cast_range
# 250: ability_4_cooldown_time_remaining
# 251: ability_4_target_type
# 252: ability_4_behavior
#
# 253: ability_5_level                                                         *
# 254: ability_5_mana_cost
# 255: ability_5_ability_damage
# 256: ability_5_cast_range
# 257: ability_5_cooldown_time_remaining
# 258: ability_5_target_type
# 259: ability_5_behavior
#
# 260: ability_6_level                                                         *
# 261: ability_6_mana_cost                                                     .
# 262: ability_6_ability_damage
# 263: ability_6_cast_range
# 264: ability_6_cooldown_time_remaining                                       .
# 265: ability_5_target_type
# 266: ability_5_behavior
#
# 267: opp_ability_1_level                                                     *
# 268: opp_ability_1_mana_cost                                                 .
# 260: opp_ability_1_ability_damage
# 270: opp_ability_1_cast_range
# 271: opp_ability_1_cooldown_time_remaining                                   .
# 272: opp_ability_1_target_type
# 273: opp_ability_1_behavior
#
# 274: opp_ability_2_level
# 275: opp_ability_2_mana_cost
# 276: opp_ability_2_ability_damage
# 277: opp_ability_2_cast_range
# 278: opp_ability_2_cooldown_time_remaining                                   .
# 279: opp_ability_2_target_type
# 280: opp_ability_2_behavior
#
# 281: opp_ability_3_level
# 282: opp_ability_3_mana_cost
# 283: opp_ability_3_ability_damage
# 284: opp_ability_3_cast_range
# 285: opp_ability_3_cooldown_time_remaining                                   .
# 286: opp_ability_3_target_type
# 287: opp_ability_3_behavior
#
# 288: opp_ability_4_level                                                     *
# 289: opp_ability_4_mana_cost
# 290: opp_ability_4_ability_damage
# 291: opp_ability_4_cast_range
# 292: opp_ability_4_cooldown_time_remaining                                   .
# 293: opp_ability_4_target_type
# 294: opp_ability_4_behavior
#
# 295: opp_ability_5_level                                                     *
# 296: opp_ability_5_mana_cost
# 297: opp_ability_5_ability_damage
# 298: opp_ability_5_cast_range
# 299: opp_ability_5_cooldown_time_remaining                                   .
# 300: opp_ability_5_target_type
# 301: opp_ability_5_behavior
#
# 302: opp_ability_6_level                                                     *
# 303: opp_ability_6_mana_cost
# 304: opp_ability_6_ability_damage
# 305: opp_ability_6_cast_range
# 306: opp_ability_6_cooldown_time_remaining                                   .
# 307: opp_ability_6_target_type
# 308: opp_ability_6_behavior
# 309: item_flask                                                              .
#
#
# Actions:
# 0: do_nothing
#
# 1: move_north
# 2: move_northeast
# 3: move_east
# 4: move_southeast
# 5: move_south
# 6: move_southwest
# 7: move_west
# 8: move_northwest
#
# 9: get_top_bounty_rune
# 10: get_bottom_bounty_rune
# 11: get_top_powerup_rune
# 12: get_bottom_powerup_rune
#
# 13: attack_enemy_hero
# 14: attack_enemy_tower
# 15: attack_creep_0 (nearest_enemy)
# 16: attack_creep_1
# 17: attack_creep_2
# 18: attack_creep_3
# 19: attack_creep_4 (farthest_enemy)
# 20: attack_creep_5 (nearest_friendly)
# 21: attack_creep_6
# 22: attack_creep_7
# 23: attack_creep_8
# 24: attack_creep_9 (farthest_friendly)
#
# 25: cast_shadowraze_1
# 26: cast_shadowraze_2
# 27: cast_shadowraze_3
# 28: cast_requiem_of_souls
#
# 29: use_healing_salve
