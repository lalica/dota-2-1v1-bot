from utils import in_range, distance, get_chance_of_physical_kill, get_chance_of_magical_kill, get_chance_of_kill, will_get_hit_by_tower, will_regen

new_location = {
    0: lambda x, y: (x, y),  # Stay
    1: lambda x, y: (x + 100, y + 100),  # North
    2: lambda x, y: (x + 100, y),  # North East
    3: lambda x, y: (x + 100, y - 100),  # East
    4: lambda x, y: (x, y - 100),  # South East
    5: lambda x, y: (x - 100, y - 100),  # South
    6: lambda x, y: (x - 100, y),  # South West
    7: lambda x, y: (x - 100, y + 100),  # West
    8: lambda x, y: (x, y + 100)}  # North West


def get_direction_if_move(x1, y1, x2, y2):
    x = x2 - x1
    y = y2 - y1
    if x > 0 and y > 0:
        return 1
    if x > 0 and y == 0:
        return 2
    if x > 0 and y < 0:
        return 3
    if x == 0 and y < 0:
        return 4
    if x < 0 and y < 0:
        return 5
    if x < 0 and y == 0:
        return 6
    if x < 0 and y > 0:
        return 7
    if x == 0 and y > 0:
        return 8


def get_direction(x1, y1, x2, y2, cast_range):
    return 0. if in_range(x1, y1, x2, y2, cast_range) \
        else get_direction_if_move(x1, y1, x2, y2)


class FeatureExtractor:
    def get_size(self):
        return 0

    def get_features(self, state, action):
        return dict()


class SimpleExtractor(FeatureExtractor):
    def get_size(self):
        return 32

    def get_features(self, state, action):
        my_x, my_y = state["me"].x, state["me"].y
        if action in range(1, 9):
            my_x, my_y = new_location[action](my_x, my_y)

        if action in range(13, 25):
            other_x, other_y = state["enemy"].x, state["enemy"].y
            if action == 14:
                other_x, other_y = state["enemy_tower"].x, state["enemy_tower"].y
            elif action == 15:
                other_x, other_y = state["enemy_creeps"][0].x, state["enemy_creeps"][0].y
            elif action == 16:
                other_x, other_y = state["enemy_creeps"][1].x, state["enemy_creeps"][1].y
            elif action == 17:
                other_x, other_y = state["enemy_creeps"][2].x, state["enemy_creeps"][2].y
            elif action == 18:
                other_x, other_y = state["enemy_creeps"][3].x, state["enemy_creeps"][3].y
            elif action == 19:
                other_x, other_y = state["enemy_creeps"][4].x, state["enemy_creeps"][4].y
            elif action == 20:
                other_x, other_y = state["ally_creeps"][0].x, state["ally_creeps"][0].y
            elif action == 21:
                other_x, other_y = state["ally_creeps"][1].x, state["ally_creeps"][1].y
            elif action == 22:
                other_x, other_y = state["ally_creeps"][2].x, state["ally_creeps"][2].y
            elif action == 23:
                other_x, other_y = state["ally_creeps"][3].x, state["ally_creeps"][3].y
            elif action == 24:
                other_x, other_y = state["ally_creeps"][4].x, state["ally_creeps"][4].y
            my_x, my_y = new_location[get_direction(my_x, my_y, other_x, other_y, state["me"].attack_range)](my_x, my_y)

        hero_range = in_range(my_x, my_y, state["enemy"].x, state["enemy"].y, state["enemy"].attack_range)
        hero_distance = 1 / (distance(my_x, my_y, state["enemy"].x, state["enemy"].y) + 1)
        in_ally_tower_range = in_range(my_x, my_y, state["ally_tower"].x, state["ally_tower"].y, state["ally_tower"].attack_range)
        ally_tower_distance = 1 / (distance(my_x, my_y, state["ally_tower"].x, state["ally_tower"].y) + 1)
        in_enemy_tower_range = in_range(my_x, my_y, state["enemy_tower"].x, state["enemy_tower"].y, state["enemy_tower"].attack_range)
        enemy_tower_distance = 1 / (distance(my_x, my_y, state["enemy_tower"].x, state["enemy_tower"].y) + 1)
        ally_creep_1_range = in_range(my_x, my_y, state["ally_creeps"][0].x, state["ally_creeps"][0].y, state["ally_creeps"][0].attack_range)
        ally_creep_2_range = in_range(my_x, my_y, state["ally_creeps"][1].x, state["ally_creeps"][1].y, state["ally_creeps"][1].attack_range)
        ally_creep_3_range = in_range(my_x, my_y, state["ally_creeps"][2].x, state["ally_creeps"][2].y, state["ally_creeps"][2].attack_range)
        ally_creep_4_range = in_range(my_x, my_y, state["ally_creeps"][3].x, state["ally_creeps"][3].y, state["ally_creeps"][3].attack_range)
        ally_creep_5_range = in_range(my_x, my_y, state["ally_creeps"][4].x, state["ally_creeps"][4].y, state["ally_creeps"][4].attack_range)
        enemy_creep_1_range = in_range(my_x, my_y, state["enemy_creeps"][0].x, state["enemy_creeps"][0].y, state["enemy_creeps"][0].attack_range)
        enemy_creep_2_range = in_range(my_x, my_y, state["enemy_creeps"][1].x, state["enemy_creeps"][1].y, state["enemy_creeps"][1].attack_range)
        enemy_creep_3_range = in_range(my_x, my_y, state["enemy_creeps"][2].x, state["enemy_creeps"][2].y, state["enemy_creeps"][2].attack_range)
        enemy_creep_4_range = in_range(my_x, my_y, state["enemy_creeps"][3].x, state["enemy_creeps"][3].y, state["enemy_creeps"][3].attack_range)
        enemy_creep_5_range = in_range(my_x, my_y, state["enemy_creeps"][4].x, state["enemy_creeps"][4].y, state["enemy_creeps"][4].attack_range)

        chance_of_hero_kill_physical = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                            state["enemy"].x, state["enemy"].y,
                                                                            state["me"].attack_range),
                                                                   state["me"].attack_damage,
                                                                   state["me"].attack_speed,
                                                                   state["enemy"].health)
        chance_of_hero_kill_magical = get_chance_of_magical_kill(state["me"], state["enemy"].x, state["enemy"].y, state["enemy"].health, action)
        chance_of_hero_kill = get_chance_of_kill(chance_of_hero_kill_physical, chance_of_hero_kill_magical, action, 13)

        chance_of_tower_kill = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                    state["enemy_tower"].x, state["enemy_tower"].y,
                                                                    state["me"].attack_range),
                                                           state["me"].attack_damage,
                                                           state["me"].attack_speed,
                                                           state["enemy_tower"].health) if action == 14 else 0.

        chance_of_ally_creep_1_kill = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                           state["ally_creeps"][0].x, state["ally_creeps"][0].y,
                                                                           state["me"].attack_range),
                                                                  state["me"].attack_damage,
                                                                  state["me"].attack_speed,
                                                                  state["ally_creeps"][0].health) if action == 15 else 0.

        chance_of_ally_creep_2_kill = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                           state["ally_creeps"][1].x, state["ally_creeps"][1].y,
                                                                           state["me"].attack_range),
                                                                  state["me"].attack_damage,
                                                                  state["me"].attack_speed,
                                                                  state["ally_creeps"][1].health) if action == 16 else 0.

        chance_of_ally_creep_3_kill = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                           state["ally_creeps"][2].x, state["ally_creeps"][2].y,
                                                                           state["me"].attack_range),
                                                                  state["me"].attack_damage,
                                                                  state["me"].attack_speed,
                                                                  state["ally_creeps"][2].health) if action == 17 else 0.

        chance_of_ally_creep_4_kill = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                           state["ally_creeps"][3].x, state["ally_creeps"][3].y,
                                                                           state["me"].attack_range),
                                                                  state["me"].attack_damage,
                                                                  state["me"].attack_speed,
                                                                  state["ally_creeps"][3].health) if action == 18 else 0.

        chance_of_ally_creep_5_kill = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                           state["ally_creeps"][4].x, state["ally_creeps"][4].y,
                                                                           state["me"].attack_range),
                                                                  state["me"].attack_damage,
                                                                  state["me"].attack_speed,
                                                                  state["ally_creeps"][4].health) if action == 19 else 0.

        chance_of_enemy_creep_1_kill_physical = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                                     state["enemy_creeps"][0].x, state["enemy_creeps"][0].y,
                                                                                     state["me"].attack_range),
                                                                            state["me"].attack_damage,
                                                                            state["me"].attack_speed,
                                                                            state["enemy_creeps"][0].health)
        chance_of_enemy_creep_1_kill_magical = get_chance_of_magical_kill(state["me"],
                                                                          state["enemy_creeps"][0].x, state["enemy_creeps"][0].y,
                                                                          state["enemy_creeps"][0].health,
                                                                          action)
        chance_of_enemy_creep_1_kill = get_chance_of_kill(chance_of_enemy_creep_1_kill_physical, chance_of_enemy_creep_1_kill_magical, action, 20)

        chance_of_enemy_creep_2_kill_physical = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                                     state["enemy_creeps"][1].x, state["enemy_creeps"][1].y,
                                                                                     state["me"].attack_range),
                                                                            state["me"].attack_damage,
                                                                            state["me"].attack_speed,
                                                                            state["enemy_creeps"][1].health)
        chance_of_enemy_creep_2_kill_magical = get_chance_of_magical_kill(state["me"],
                                                                          state["enemy_creeps"][1].x, state["enemy_creeps"][1].y,
                                                                          state["enemy_creeps"][1].health,
                                                                          action)
        chance_of_enemy_creep_2_kill = get_chance_of_kill(chance_of_enemy_creep_2_kill_physical, chance_of_enemy_creep_2_kill_magical, action, 21)

        chance_of_enemy_creep_3_kill_physical = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                                     state["enemy_creeps"][2].x, state["enemy_creeps"][2].y,
                                                                                     state["me"].attack_range),
                                                                            state["me"].attack_damage,
                                                                            state["me"].attack_speed,
                                                                            state["enemy_creeps"][2].health)
        chance_of_enemy_creep_3_kill_magical = get_chance_of_magical_kill(state["me"],
                                                                          state["enemy_creeps"][2].x, state["enemy_creeps"][2].y,
                                                                          state["enemy_creeps"][2].health,
                                                                          action)
        chance_of_enemy_creep_3_kill = get_chance_of_kill(chance_of_enemy_creep_3_kill_physical, chance_of_enemy_creep_3_kill_magical, action, 22)

        chance_of_enemy_creep_4_kill_physical = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                                     state["enemy_creeps"][3].x, state["enemy_creeps"][3].y,
                                                                                     state["me"].attack_range),
                                                                            state["me"].attack_damage,
                                                                            state["me"].attack_speed,
                                                                            state["enemy_creeps"][3].health)
        chance_of_enemy_creep_4_kill_magical = get_chance_of_magical_kill(state["me"],
                                                                          state["enemy_creeps"][3].x, state["enemy_creeps"][3].y,
                                                                          state["enemy_creeps"][3].health,
                                                                          action)
        chance_of_enemy_creep_4_kill = get_chance_of_kill(chance_of_enemy_creep_4_kill_physical, chance_of_enemy_creep_4_kill_magical, action, 23)

        chance_of_enemy_creep_5_kill_physical = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                                     state["enemy_creeps"][4].x, state["enemy_creeps"][4].y,
                                                                                     state["me"].attack_range),
                                                                            state["me"].attack_damage,
                                                                            state["me"].attack_speed,
                                                                            state["enemy_creeps"][4].health)
        chance_of_enemy_creep_5_kill_magical = get_chance_of_magical_kill(state["me"],
                                                                          state["enemy_creeps"][4].x, state["enemy_creeps"][4].y,
                                                                          state["enemy_creeps"][4].health,
                                                                          action)
        chance_of_enemy_creep_5_kill = get_chance_of_kill(chance_of_enemy_creep_5_kill_physical, chance_of_enemy_creep_5_kill_magical, action, 24)
        tower_hit = will_get_hit_by_tower(my_x, my_y,
                                          state["ally_creeps"][0].x, state["ally_creeps"][0].y,
                                          state["ally_creeps"][1].x, state["ally_creeps"][1].y,
                                          state["ally_creeps"][2].x, state["ally_creeps"][2].y,
                                          state["ally_creeps"][3].x, state["ally_creeps"][3].y,
                                          state["ally_creeps"][4].x, state["ally_creeps"][4].y,
                                          state["enemy_tower"].x, state["enemy_tower"].y,
                                          state["enemy_tower"].attack_range)
        distance_from_mid = 1 / (distance(my_x, my_y, state["game_data"].middle_x, state["game_data"].middle_y) + 1)
        regen = will_regen(action, state["game_data"].fountain_x, state["game_data"].fountain_y, my_x, my_y)

        features = {'health': (state["me"].health + (40 if action == 29 else 0))/state["me"].max_health,
                    'in_hero_attack_range': hero_range,
                    'hero_distance': hero_distance,
                    'in_ally_tower_range': in_ally_tower_range,
                    'ally_tower_distance': ally_tower_distance,
                    'in_enemy_tower_range': in_enemy_tower_range,
                    'enemy_tower_distance': enemy_tower_distance,
                    'in_ally_creep_1_range': ally_creep_1_range,
                    'in_ally_creep_2_range': ally_creep_2_range,
                    'in_ally_creep_3_range': ally_creep_3_range,
                    'in_ally_creep_4_range': ally_creep_4_range,
                    'in_ally_creep_5_range': ally_creep_5_range,
                    'in_enemy_creep_1_range': enemy_creep_1_range,
                    'in_enemy_creep_2_range': enemy_creep_2_range,
                    'in_enemy_creep_3_range': enemy_creep_3_range,
                    'in_enemy_creep_4_range': enemy_creep_4_range,
                    'in_enemy_creep_5_range': enemy_creep_5_range,
                    'chance_of_hero_kill': chance_of_hero_kill,
                    'chance_of_tower_kill': chance_of_tower_kill,
                    'chance_of_ally_creep_1_kill': chance_of_ally_creep_1_kill,
                    'chance_of_ally_creep_2_kill': chance_of_ally_creep_2_kill,
                    'chance_of_ally_creep_3_kill': chance_of_ally_creep_3_kill,
                    'chance_of_ally_creep_4_kill': chance_of_ally_creep_4_kill,
                    'chance_of_ally_creep_5_kill': chance_of_ally_creep_5_kill,
                    'chance_of_enemy_creep_1_kill': chance_of_enemy_creep_1_kill,
                    'chance_of_enemy_creep_2_kill': chance_of_enemy_creep_2_kill,
                    'chance_of_enemy_creep_3_kill': chance_of_enemy_creep_3_kill,
                    'chance_of_enemy_creep_4_kill': chance_of_enemy_creep_4_kill,
                    'chance_of_enemy_creep_5_kill': chance_of_enemy_creep_5_kill,
                    'will_get_hit_by_tower': tower_hit,
                    'distance_from_mid': distance_from_mid,
                    'will_regen': regen}

        return features


class OnlyStateExtractor(FeatureExtractor):
    def get_size(self):
        return 60

    def get_features(self, state, _):
        my_x, my_y = state["me"].x, state["me"].y
        hero_range = in_range(my_x, my_y, state["enemy"].x, state["enemy"].y, state["enemy"].attack_range)
        hero_distance = 1 / (distance(my_x, my_y, state["enemy"].x, state["enemy"].y) + 1)
        in_ally_tower_range = in_range(my_x, my_y, state["ally_tower"].x, state["ally_tower"].y, state["ally_tower"].attack_range)
        ally_tower_distance = 1 / (distance(my_x, my_y, state["ally_tower"].x, state["ally_tower"].y) + 1)
        in_enemy_tower_range = in_range(my_x, my_y, state["enemy_tower"].x, state["enemy_tower"].y, state["enemy_tower"].attack_range)
        enemy_tower_distance = 1 / (distance(my_x, my_y, state["enemy_tower"].x, state["enemy_tower"].y) + 1)
        ally_creep_1_range = in_range(my_x, my_y, state["ally_creeps"][0].x, state["ally_creeps"][0].y, state["ally_creeps"][0].attack_range)
        ally_creep_2_range = in_range(my_x, my_y, state["ally_creeps"][1].x, state["ally_creeps"][1].y, state["ally_creeps"][1].attack_range)
        ally_creep_3_range = in_range(my_x, my_y, state["ally_creeps"][2].x, state["ally_creeps"][2].y, state["ally_creeps"][2].attack_range)
        ally_creep_4_range = in_range(my_x, my_y, state["ally_creeps"][3].x, state["ally_creeps"][3].y, state["ally_creeps"][3].attack_range)
        ally_creep_5_range = in_range(my_x, my_y, state["ally_creeps"][4].x, state["ally_creeps"][4].y, state["ally_creeps"][4].attack_range)
        enemy_creep_1_range = in_range(my_x, my_y, state["enemy_creeps"][0].x, state["enemy_creeps"][0].y, state["enemy_creeps"][0].attack_range)
        enemy_creep_2_range = in_range(my_x, my_y, state["enemy_creeps"][1].x, state["enemy_creeps"][1].y, state["enemy_creeps"][1].attack_range)
        enemy_creep_3_range = in_range(my_x, my_y, state["enemy_creeps"][2].x, state["enemy_creeps"][2].y, state["enemy_creeps"][2].attack_range)
        enemy_creep_4_range = in_range(my_x, my_y, state["enemy_creeps"][3].x, state["enemy_creeps"][3].y, state["enemy_creeps"][3].attack_range)
        enemy_creep_5_range = in_range(my_x, my_y, state["enemy_creeps"][4].x, state["enemy_creeps"][4].y, state["enemy_creeps"][4].attack_range)

        chance_of_hero_kill_physical = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                            state["enemy"].x, state["enemy"].y,
                                                                            state["me"].attack_range),
                                                                   state["me"].attack_damage,
                                                                   state["me"].attack_speed,
                                                                   state["enemy"].health)
        chance_of_hero_kill_shadow_raze_1 = get_chance_of_magical_kill(state["me"], state["enemy"].x, state["enemy"].y, state["enemy"].health, 25)
        chance_of_hero_kill_shadow_raze_2 = get_chance_of_magical_kill(state["me"], state["enemy"].x, state["enemy"].y, state["enemy"].health, 26)
        chance_of_hero_kill_shadow_raze_3 = get_chance_of_magical_kill(state["me"], state["enemy"].x, state["enemy"].y, state["enemy"].health, 27)
        chance_of_hero_kill_ult = get_chance_of_magical_kill(state["me"], state["enemy"].x, state["enemy"].y, state["enemy"].health, 28)

        chance_of_tower_kill = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                    state["enemy_tower"].x, state["enemy_tower"].y,
                                                                    state["me"].attack_range),
                                                           state["me"].attack_damage,
                                                           state["me"].attack_speed,
                                                           state["enemy_tower"].health)

        chance_of_ally_creep_1_kill = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                           state["ally_creeps"][0].x, state["ally_creeps"][0].y,
                                                                           state["me"].attack_range),
                                                                  state["me"].attack_damage,
                                                                  state["me"].attack_speed,
                                                                  state["ally_creeps"][0].health)

        chance_of_ally_creep_2_kill = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                           state["ally_creeps"][1].x, state["ally_creeps"][1].y,
                                                                           state["me"].attack_range),
                                                                  state["me"].attack_damage,
                                                                  state["me"].attack_speed,
                                                                  state["ally_creeps"][1].health)

        chance_of_ally_creep_3_kill = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                           state["ally_creeps"][2].x, state["ally_creeps"][2].y,
                                                                           state["me"].attack_range),
                                                                  state["me"].attack_damage,
                                                                  state["me"].attack_speed,
                                                                  state["ally_creeps"][2].health)

        chance_of_ally_creep_4_kill = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                           state["ally_creeps"][3].x, state["ally_creeps"][3].y,
                                                                           state["me"].attack_range),
                                                                  state["me"].attack_damage,
                                                                  state["me"].attack_speed,
                                                                  state["ally_creeps"][3].health)

        chance_of_ally_creep_5_kill = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                           state["ally_creeps"][4].x, state["ally_creeps"][4].y,
                                                                           state["me"].attack_range),
                                                                  state["me"].attack_damage,
                                                                  state["me"].attack_speed,
                                                                  state["ally_creeps"][4].health)

        chance_of_enemy_creep_1_kill_physical = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                                     state["enemy_creeps"][0].x, state["enemy_creeps"][0].y,
                                                                                     state["me"].attack_range),
                                                                            state["me"].attack_damage,
                                                                            state["me"].attack_speed,
                                                                            state["enemy_creeps"][0].health)
        chance_of_enemy_creep_1_kill_shadow_raze_1 = get_chance_of_magical_kill(state["me"],
                                                                                state["enemy_creeps"][0].x, state["enemy_creeps"][0].y,
                                                                                state["enemy_creeps"][0].health,
                                                                                25)
        chance_of_enemy_creep_1_kill_shadow_raze_2 = get_chance_of_magical_kill(state["me"],
                                                                                state["enemy_creeps"][0].x, state["enemy_creeps"][0].y,
                                                                                state["enemy_creeps"][0].health,
                                                                                26)
        chance_of_enemy_creep_1_kill_shadow_raze_3 = get_chance_of_magical_kill(state["me"],
                                                                                state["enemy_creeps"][0].x, state["enemy_creeps"][0].y,
                                                                                state["enemy_creeps"][0].health,
                                                                                27)
        chance_of_enemy_creep_1_kill_ult = get_chance_of_magical_kill(state["me"],
                                                                      state["enemy_creeps"][0].x, state["enemy_creeps"][0].y,
                                                                      state["enemy_creeps"][0].health,
                                                                      28)

        chance_of_enemy_creep_2_kill_physical = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                                     state["enemy_creeps"][1].x, state["enemy_creeps"][1].y,
                                                                                     state["me"].attack_range),
                                                                            state["me"].attack_damage,
                                                                            state["me"].attack_speed,
                                                                            state["enemy_creeps"][1].health)
        chance_of_enemy_creep_2_kill_shadow_raze_1 = get_chance_of_magical_kill(state["me"],
                                                                                state["enemy_creeps"][1].x, state["enemy_creeps"][1].y,
                                                                                state["enemy_creeps"][1].health,
                                                                                25)
        chance_of_enemy_creep_2_kill_shadow_raze_2 = get_chance_of_magical_kill(state["me"],
                                                                                state["enemy_creeps"][1].x, state["enemy_creeps"][1].y,
                                                                                state["enemy_creeps"][1].health,
                                                                                26)
        chance_of_enemy_creep_2_kill_shadow_raze_3 = get_chance_of_magical_kill(state["me"],
                                                                                state["enemy_creeps"][1].x, state["enemy_creeps"][1].y,
                                                                                state["enemy_creeps"][1].health,
                                                                                27)
        chance_of_enemy_creep_2_kill_ult = get_chance_of_magical_kill(state["me"],
                                                                      state["enemy_creeps"][1].x, state["enemy_creeps"][1].y,
                                                                      state["enemy_creeps"][1].health,
                                                                      28)

        chance_of_enemy_creep_3_kill_physical = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                                     state["enemy_creeps"][2].x, state["enemy_creeps"][2].y,
                                                                                     state["me"].attack_range),
                                                                            state["me"].attack_damage,
                                                                            state["me"].attack_speed,
                                                                            state["enemy_creeps"][2].health)
        chance_of_enemy_creep_3_kill_shadow_raze_1 = get_chance_of_magical_kill(state["me"],
                                                                                state["enemy_creeps"][2].x, state["enemy_creeps"][2].y,
                                                                                state["enemy_creeps"][2].health,
                                                                                25)
        chance_of_enemy_creep_3_kill_shadow_raze_2 = get_chance_of_magical_kill(state["me"],
                                                                                state["enemy_creeps"][2].x, state["enemy_creeps"][2].y,
                                                                                state["enemy_creeps"][2].health,
                                                                                26)
        chance_of_enemy_creep_3_kill_shadow_raze_3 = get_chance_of_magical_kill(state["me"],
                                                                                state["enemy_creeps"][2].x, state["enemy_creeps"][2].y,
                                                                                state["enemy_creeps"][2].health,
                                                                                27)
        chance_of_enemy_creep_3_kill_ult = get_chance_of_magical_kill(state["me"],
                                                                      state["enemy_creeps"][2].x, state["enemy_creeps"][2].y,
                                                                      state["enemy_creeps"][2].health,
                                                                      28)

        chance_of_enemy_creep_4_kill_physical = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                                     state["enemy_creeps"][3].x, state["enemy_creeps"][3].y,
                                                                                     state["me"].attack_range),
                                                                            state["me"].attack_damage,
                                                                            state["me"].attack_speed,
                                                                            state["enemy_creeps"][3].health)
        chance_of_enemy_creep_4_kill_shadow_raze_1 = get_chance_of_magical_kill(state["me"],
                                                                                state["enemy_creeps"][3].x, state["enemy_creeps"][3].y,
                                                                                state["enemy_creeps"][3].health,
                                                                                25)
        chance_of_enemy_creep_4_kill_shadow_raze_2 = get_chance_of_magical_kill(state["me"],
                                                                                state["enemy_creeps"][3].x, state["enemy_creeps"][3].y,
                                                                                state["enemy_creeps"][3].health,
                                                                                26)
        chance_of_enemy_creep_4_kill_shadow_raze_3 = get_chance_of_magical_kill(state["me"],
                                                                                state["enemy_creeps"][3].x, state["enemy_creeps"][3].y,
                                                                                state["enemy_creeps"][3].health,
                                                                                27)
        chance_of_enemy_creep_4_kill_ult = get_chance_of_magical_kill(state["me"],
                                                                      state["enemy_creeps"][3].x, state["enemy_creeps"][3].y,
                                                                      state["enemy_creeps"][3].health,
                                                                      28)

        chance_of_enemy_creep_5_kill_physical = get_chance_of_physical_kill(in_range(state["me"].x, state["me"].y,
                                                                                     state["enemy_creeps"][4].x, state["enemy_creeps"][4].y,
                                                                                     state["me"].attack_range),
                                                                            state["me"].attack_damage,
                                                                            state["me"].attack_speed,
                                                                            state["enemy_creeps"][4].health)
        chance_of_enemy_creep_5_kill_shadow_raze_1 = get_chance_of_magical_kill(state["me"],
                                                                                state["enemy_creeps"][4].x, state["enemy_creeps"][4].y,
                                                                                state["enemy_creeps"][4].health,
                                                                                25)
        chance_of_enemy_creep_5_kill_shadow_raze_2 = get_chance_of_magical_kill(state["me"],
                                                                                state["enemy_creeps"][4].x, state["enemy_creeps"][4].y,
                                                                                state["enemy_creeps"][4].health,
                                                                                26)
        chance_of_enemy_creep_5_kill_shadow_raze_3 = get_chance_of_magical_kill(state["me"],
                                                                                state["enemy_creeps"][4].x, state["enemy_creeps"][4].y,
                                                                                state["enemy_creeps"][4].health,
                                                                                27)
        chance_of_enemy_creep_5_kill_ult = get_chance_of_magical_kill(state["me"],
                                                                      state["enemy_creeps"][4].x, state["enemy_creeps"][4].y,
                                                                      state["enemy_creeps"][4].health,
                                                                      28)
        tower_hit = will_get_hit_by_tower(my_x, my_y,
                                          state["ally_creeps"][0].x, state["ally_creeps"][0].y,
                                          state["ally_creeps"][1].x, state["ally_creeps"][1].y,
                                          state["ally_creeps"][2].x, state["ally_creeps"][2].y,
                                          state["ally_creeps"][3].x, state["ally_creeps"][3].y,
                                          state["ally_creeps"][4].x, state["ally_creeps"][4].y,
                                          state["enemy_tower"].x, state["enemy_tower"].y,
                                          state["enemy_tower"].attack_range)
        distance_from_mid = 1 / (distance(my_x, my_y, state["game_data"].middle_x, state["game_data"].middle_y) + 1)

        return [state["me"].health / state["me"].max_health,
                state["me"].mana / state["me"].max_mana,
                state["me"].level / state["enemy"].level,
                state["me"].x / 7000,
                state["me"].y / 7000,
                1. * hero_range,
                1. * hero_distance,
                1. * in_ally_tower_range,
                1. * ally_tower_distance,
                1. * in_enemy_tower_range,
                1. * enemy_tower_distance,
                1. * ally_creep_1_range,
                1. * ally_creep_2_range,
                1. * ally_creep_3_range,
                1. * ally_creep_4_range,
                1. * ally_creep_5_range,
                1. * enemy_creep_1_range,
                1. * enemy_creep_2_range,
                1. * enemy_creep_3_range,
                1. * enemy_creep_4_range,
                1. * enemy_creep_5_range,
                1. * chance_of_hero_kill_physical,
                1. * chance_of_hero_kill_shadow_raze_1,
                1. * chance_of_hero_kill_shadow_raze_2,
                1. * chance_of_hero_kill_shadow_raze_3,
                1. * chance_of_hero_kill_ult,
                1. * chance_of_tower_kill,
                1. * chance_of_ally_creep_1_kill,
                1. * chance_of_ally_creep_2_kill,
                1. * chance_of_ally_creep_3_kill,
                1. * chance_of_ally_creep_4_kill,
                1. * chance_of_ally_creep_5_kill,
                1. * chance_of_enemy_creep_1_kill_physical,
                1. * chance_of_enemy_creep_1_kill_shadow_raze_1,
                1. * chance_of_enemy_creep_1_kill_shadow_raze_2,
                1. * chance_of_enemy_creep_1_kill_shadow_raze_3,
                1. * chance_of_enemy_creep_1_kill_ult,
                1. * chance_of_enemy_creep_2_kill_physical,
                1. * chance_of_enemy_creep_2_kill_shadow_raze_1,
                1. * chance_of_enemy_creep_2_kill_shadow_raze_2,
                1. * chance_of_enemy_creep_2_kill_shadow_raze_3,
                1. * chance_of_enemy_creep_2_kill_ult,
                1. * chance_of_enemy_creep_3_kill_physical,
                1. * chance_of_enemy_creep_3_kill_shadow_raze_1,
                1. * chance_of_enemy_creep_3_kill_shadow_raze_2,
                1. * chance_of_enemy_creep_3_kill_shadow_raze_3,
                1. * chance_of_enemy_creep_3_kill_ult,
                1. * chance_of_enemy_creep_4_kill_physical,
                1. * chance_of_enemy_creep_4_kill_shadow_raze_1,
                1. * chance_of_enemy_creep_4_kill_shadow_raze_2,
                1. * chance_of_enemy_creep_4_kill_shadow_raze_3,
                1. * chance_of_enemy_creep_4_kill_ult,
                1. * chance_of_enemy_creep_5_kill_physical,
                1. * chance_of_enemy_creep_5_kill_shadow_raze_1,
                1. * chance_of_enemy_creep_5_kill_shadow_raze_2,
                1. * chance_of_enemy_creep_5_kill_shadow_raze_3,
                1. * chance_of_enemy_creep_5_kill_ult,
                1. * tower_hit,
                1. * distance_from_mid,
                1. * state["game_data"].healing_salve_available]
