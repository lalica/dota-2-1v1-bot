import random
import math


def flip_coin(p):
    return random.random() < p


def distance(x1, y1, x2, y2):
    return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)


def in_range(x1, y1, x2, y2, cast_range):
    return 1. if distance(x1, y1, x2, y2) <= cast_range else 0.


def chance_of_physical_kill(attack_dmg, attack_speed, health):
    if health == 0:
        return 0.
    return 1. - max(health - attack_dmg * attack_speed * 0.007, 0.) / health


def get_chance_of_physical_kill(target_in_range, attack_damage, attack_speed, health):
    if target_in_range:
        return chance_of_physical_kill(attack_damage, attack_speed, health)
    return 0.


def will_shadowraze_hit(x1, y1, x2, y2, shadowraze_range):
    if shadowraze_range - 250 <= distance(x1, y1, x2, y2) <= shadowraze_range + 250:
        return 1.
    return 0.


def chance_of_magical_kill(damage, health):
    if health == 0:
        return 0.
    return 1. - max(health - damage, 0.) / health


def get_chance_of_magical_kill(me, x2, y2, health, action):
    if health <= 0:
        return 0

    if action == 28:
        if in_range(me.x, me.y, x2, y2, me.ult_cast_range):
            return chance_of_magical_kill(me.ult_damage, health)

    ability_range = me.ability_1_cast_range if action == 25 else (me.ability_2_cast_range if action == 26 else me.ability_3_cast_range)
    if will_shadowraze_hit(me.x, me.y, x2, y2, ability_range):
        return chance_of_magical_kill((1.08 if me.level >= 10 else 1) * me.ability_damage, health)

    return 0.


def get_chance_of_kill(physical_kill, magical_kill, action, physical):
    return physical_kill if action == physical \
        else (magical_kill if action in [25, 26, 27, 28] else 0.)


def will_get_hit_by_tower(me_1, me_2,
                          creep1_1, creep1_2,
                          creep2_1, creep2_2,
                          creep3_1, creep3_2,
                          creep4_1, creep4_2,
                          creep5_1, creep5_2,
                          enemy_tower_x, enemy_tower_y,
                          tower_attack_range):
    if not in_range(me_1, me_2, enemy_tower_x, enemy_tower_y, tower_attack_range):
        return 0.

    my_dist = distance(me_1, me_2, enemy_tower_x, enemy_tower_y)
    if (distance(creep1_1, creep1_2, enemy_tower_x, enemy_tower_y) > my_dist and
            distance(creep2_1, creep2_2, enemy_tower_x, enemy_tower_y) > my_dist and
            distance(creep3_1, creep3_2, enemy_tower_x, enemy_tower_y) > my_dist and
            distance(creep4_1, creep4_2, enemy_tower_x, enemy_tower_y) > my_dist and
            distance(creep5_1, creep5_2, enemy_tower_x, enemy_tower_y) > my_dist):
        return 1.
    return 0.


def should_regen(health):
    return 1. if health <= 200 else 0.


def will_regen(action, fountain_x, fountain_y, x, y):
    return 1. if action == 29 or in_range(x, y, fountain_x, fountain_y, 400) else 0.


def can_cast(level, mana_cost, current_mana, cooldown):
    return level > 0 and cooldown == 0 and current_mana >= mana_cost


def argmax_pairs(pairs):
    return max(pairs, key=lambda x: x[1])[0]


def argmax(array):
    return argmax_pairs(enumerate(array))


def sorted_argmax(array):
    return [x[0] for x in sorted(enumerate(array), key=lambda x: x[1], reverse=True)]
