## Setup instructions:

1. Follow instructions on [this](https://web.cs.dal.ca/~dota2/?page_id=307) link for Breezy server and Dota 2 setup
2. Make sure you have tensorflow python package installed (python version 3.7)
3. Start the agent using command `python breezy-agent.py` (alternatively use pypy, python3, or double click the file)
4. Open `http://localhost:8085/` if you wish to control the hero
5. Enjoy the game
6. If you wish to change the RL method used change the line `21` in the `breezy-agent.py` file


## File description:

1. `breezy-agent.py` - file which contains the logic for comunication with the breezy-server. From there the agent actions are called.
2. `dota_environment.py` - file which contains the logic for Dota 2 environment (getting state and rewards)
3. `feature_extractors.py` - file which contatins the logic of extracting the useful features from the state
4. `neural_network.py` - file which contains the simple implementation of the artificial neural network which ended up not being used
5. `q_learning.py` - file containing the logic for reinforcement learning and all its instances
6. `utils.py` - file containing some useful functions

## Breezy setup:

If the link for breezy setup stops working follow this instructions:

1. Download breezy files from [breezy gitlab](https://gitlab.com/LivinBreezy/project-breezy)
2. Install Steam and Dota 2
3. From the Library window, right click Dota 2 in your game list. Click Properties, and then click the DLC tab at the top of the window that appears. In the DLC list, check Reborn Beta and Workshop Tools DLC, then click Close to close the window. This will prompt your Steam to download more data for Dota 2. 
4. Go to your library in Steam, click on Dota 2, click on Workshop tab, search for Breezy Addon and subscribe
5. Inside breezy server folder run `.\gradlew.bat shadowjar`
6. After that run `java -jar .\build\libs\server-1.0-SNAPSHOT-all.jar -conf conf.json` to start the server
